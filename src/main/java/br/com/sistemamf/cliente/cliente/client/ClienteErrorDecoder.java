package br.com.sistemamf.cliente.cliente.client;

import br.com.sistemamf.cliente.cliente.exception.ClientNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new ClientNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}