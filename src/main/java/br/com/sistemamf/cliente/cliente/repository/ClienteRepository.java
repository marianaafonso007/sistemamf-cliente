package br.com.sistemamf.cliente.cliente.repository;

import br.com.sistemamf.cliente.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

}
