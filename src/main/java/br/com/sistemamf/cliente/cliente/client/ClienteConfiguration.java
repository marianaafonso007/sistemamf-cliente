package br.com.sistemamf.cliente.cliente.client;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteErrorDecoder();
    }

}
